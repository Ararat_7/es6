# README #

This is a Javascript module, which allows you to use a range of new methods and properties presented in [ECMAScript 6](http://en.wikipedia.org/wiki/ECMAScript). 
All test cases are based on the ones represented in the following document: [Understanding ECMAScript 6](https://leanpub.com/understandinges6/read).

Here is the complete list of supported methods and properties:

* [String.prototype.includes()](https://leanpub.com/understandinges6/read#leanpub-auto-includes-startswith-endswith) method
* [String.prototype.startsWith()](https://leanpub.com/understandinges6/read#leanpub-auto-includes-startswith-endswith) method
* [String.prototype.endsWith()](https://leanpub.com/understandinges6/read#leanpub-auto-includes-startswith-endswith) method
* [String.prototype.repeat()](https://leanpub.com/understandinges6/read#leanpub-auto-repeat) method
* [RegExp.prototype.flags](https://leanpub.com/understandinges6/read#leanpub-auto-the-flags-property) property
* [Object.is()](https://leanpub.com/understandinges6/read#leanpub-auto-objectis) method
* [Number.isFinite()](https://leanpub.com/understandinges6/read#leanpub-auto-isfinite-and-isnan) method
* [Number.isNaN()](https://leanpub.com/understandinges6/read#leanpub-auto-isfinite-and-isnan) method

### What is this repository for? ###

* This module adds support of new methods and properties of ECMAScript 6
* 0.0.1

### How do I get set up? ###

* Download the **es6.js** script
* Include it in your HTML before all your scripts
* Turn on the support by calling **on()** method of **ES6** object, e.g. **ES6.on()**
* And that's all! ES6 rules :)

### Running tests ### 

* Clone this repository and run the tests by opening the **test.html** from *test* folder

**This module has no dependencies**

### Contribution ###

* You can contribute to this module by helping to solve the problems for the rest of the new functionality of ECMAScript 6 (e.g. *let* keyword, *arrow* functions, etc)

### Who do I talk to? ###

* @Karlen91 on Twitter









*The image logo is taken from sencha.io.*