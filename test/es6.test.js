var expect = chai.expect;

/*jshint expr:true*/

describe('ES6', function() {
    'use strict';

    var message = 'Hello world!';

    before(function() {
        ES6.on(); // Turn on EcmaScript 6 support
    });

    describe('includes() method', function() {
        it('should return true if the given text is found anywhere within the string or false if not.', function() {
            expect(message.includes("Hello")).to.be.true;
            expect(message.includes("x")).to.be.false;
            expect(message.includes("o", 8)).to.be.false;
        });

        it('should throw an IllegalArgumentException if passed not a string', function() {
            expect(message.includes).to.throw(IllegalArgumentException);
        });
    });

    describe('startsWith() method', function() {
        it('should return true if the given text is found at the beginning of the string or false if not.', function() {
            expect(message.startsWith("Hello")).to.be.true;
            expect(message.startsWith("o")).to.be.false;
            expect(message.startsWith("o", 4)).to.be.true;
        });

        it('should throw an IllegalArgumentException if passed not a string', function() {
            expect(message.startsWith).to.throw(IllegalArgumentException);
        });
    });

    describe('endsWith() method', function() {
        it('should return true if the given text is found at the end of the string or false if not.', function() {
            expect(message.endsWith("!")).to.be.true;
            expect(message.endsWith("world!")).to.be.true;
            expect(message.endsWith("o", 8)).to.be.true;
        });

        it('should throw an IllegalArgumentException if passed not a string', function() {
            expect(message.endsWith).to.throw(IllegalArgumentException);
        });
    });

    describe('repeat() method', function() {
        it('should return a new string that has the original string repeated the specified number of times.', function() {
            expect("x".repeat(3)).to.equal('xxx');
            expect("hello".repeat(2)).to.equal('hellohello');
            expect("abc".repeat(4)).to.equal('abcabcabcabc');
        });

        it('should throw an IllegalArgumentException if passed not a number', function() {
            expect(message.endsWith).to.throw(IllegalArgumentException);
        });
    });

    describe('flags property', function() {
        it('should return the string representation of any flags applied to a regular expression', function() {
            expect(/abc/g.flags).to.equal('g');
            expect(/abc/gmi.flags).to.equal('gim');     // auto-ordering of the flags happens here
        });
    });

    describe('is() method', function() {
        it('should perform more accurate check, e.g. +0 !== -0 and NaN === NaN', function() {
            expect(Object.is(+0, -0)).to.be.false;
            expect(Object.is(NaN, NaN)).to.be.true;
            expect(Object.is(5, 5)).to.be.true;
            expect(Object.is(5, '5')).to.be.false; 
        });
    });

    describe('isFinite() method', function() {
        it('should perform the same comparison as isFinite() global function but only for number values.', function() {
            expect(Number.isFinite(25)).to.be.true;
            expect(Number.isFinite('25')).to.be.false;
        });
    });

    describe('isNaN() method', function() {
        it('should perform the same comparison as isNaN() global function but only for number values.', function() {
            expect(Number.isNaN(NaN)).to.be.true;
            expect(Number.isNaN('NaN')).to.be.false;
        });
    });

});