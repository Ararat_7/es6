;
(function(exports) {
    'use strict';

    // IllegalArgument Exception

    function IllegalArgumentException(message) {
        this.message = message;
    }

    function ES6() {}

    exports.ES6 = new ES6();
    exports.IllegalArgumentException = IllegalArgumentException;

    ES6.prototype.on = function() {

        // includes() method

        Object.defineProperty(String.prototype, 'includes', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(str, start) {

                if (typeof str !== 'string') {
                    throw new IllegalArgumentException('Argument should be a string.');
                }

                start = start || 0;

                return this.indexOf(str, start) > start - 1;
            }
        });

        // startsWith() method

        Object.defineProperty(String.prototype, 'startsWith', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(str, start) {

                if (typeof str !== 'string') {
                    throw new IllegalArgumentException('Argument should be a string.');
                }

                start = start || 0;

                return this.indexOf(str, start) === start;
            }
        });

        // endsWith() method

        Object.defineProperty(String.prototype, 'endsWith', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(str, end) {
                var _this = this;

                if (typeof str !== 'string') {
                    throw new IllegalArgumentException('Argument should be a string.');
                }

                _this = _this.substr(0, end);
                end = end || _this.length;

                return _this.indexOf(str, _this.length - str.length) !== -1;
            }
        });

        // repeat() method

        Object.defineProperty(String.prototype, 'repeat', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(times) {
                times = parseInt(times);

                if (isNaN(times)) {
                    throw new IllegalArgumentException('Argument should be a number.');
                }

                return new Array(times + 1).join(this);
            }
        });

        // flags property

        Object.defineProperty(RegExp.prototype, 'flags', {
            enumerable: true,
            configurable: true,
            get: function() {
                var regexp = this.toString();
                return regexp.substring(regexp.lastIndexOf("/") + 1, regexp.length);
            }
        });

        // is() method

        Object.defineProperty(Object, 'is', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(op1, op2) {
                if (op1 === 0 && op2 === 0) {
                    return 1/op1 === 1/op2;
                }

                if (isNaN(op1) && isNaN(op2)) {
                    return true;
                }

                return op1 === op2;
            }
        });

        // isFinite() method

        Object.defineProperty(Number, 'isFinite', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(op) {
                if (typeof op !== 'number') {
                    return false;
                }

                return op !== Infinity && op !== -Infinity;
            }
        });

        // isNaN() method

        Object.defineProperty(Number, 'isNaN', {
            writable: false,
            enumerable: false,
            configurable: true,
            value: function(op) {
                if (typeof op !== 'number') {
                    return false;
                }

                return isNaN(op);
            }
        });
    };

})(this);